using CrewChiefV4.GameState;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CrewChiefV4
{
    //public enum GameEnum
    //{
    //    RACE_ROOM, PCARS2, PCARS_64BIT, PCARS_32BIT, PCARS_NETWORK, PCARS2_NETWORK, RF1, ASSETTO_64BIT, ASSETTO_64BIT_RALLY, ASSETTO_32BIT,
    //    RF2_64BIT, LMU, IRACING, F1_2018, F1_2019, F1_2020, F1_2021, ACC, AMS2, AMS2_NETWORK, PCARS3, RBR, DIRT, DIRT_2, GTR2, UNKNOWN,
    //    NONE, /* this allows CC macros to run when an unsupported game is being played, it's selectable from the Games list */
    //    ANY   /* this allows CC macros to be defined that apply to all supported games, it's only selectable from the macro UI */
    //}
    public partial class GameDefinition
    {
        // Also add any new GameDefinitions to getAllGameDefinitions()
        private static readonly string showOnlyTheseGames = UserSettings.GetUserSettings().getString("limit_available_games");

        private static List<GameDefinition> filterAvailableGames(List<GameDefinition> gameDefinitions)
        {
            if (showOnlyTheseGames != null && showOnlyTheseGames.Length > 0)
            {
                try
                {
                    string[] filters = showOnlyTheseGames.Split(',');
                    HashSet<GameDefinition> filtered = new HashSet<GameDefinition>();
                    bool anyMatch = false;
                    foreach (string filterItem in filters)
                    {
                        Boolean matched = false;
                        String filter = filterItem.Trim();
                        if (filter.Length > 0)
                        {
                            foreach (GameDefinition gameDefinition in gameDefinitions)
                            {
                                if (filterItem.Length > 0 && (
                                    gameDefinition.friendlyName.Equals(filter) || gameDefinition.lookupName.Equals(filter) || gameDefinition.commandLineName.Equals(filter)))
                                {
                                    filtered.Add(gameDefinition);
                                    matched = true;
                                    anyMatch = true;
                                    break;
                                }
                            }
                            if (!matched)
                            {
                                // no match for this filter, see if we can do an approx match
                                string filterLower = filter.ToLower();
                                foreach (GameDefinition gameDefinition in gameDefinitions)
                                {
                                    if (filterItem.Length > 0 && gameDefinition.alternativeFilterNames != null
                                        && gameDefinition.alternativeFilterNames.Contains(filterLower))
                                    {
                                        filtered.Add(gameDefinition);
                                        matched = true;
                                        anyMatch = true;
                                        Log.Warning($"Limit available games filter '{filter}' should be '{gameDefinition.lookupName}'");
                                        break;
                                    }
                                }
                                if (!matched)
                                {
                                    Log.Error($"Limit available games filter term '{filter}' not recognised");
                                }
                            }
                        }
                    }
                    if (anyMatch)
                    {
                        return filtered.ToList();
                    }
                }
                catch (Exception e) { Log.Exception(e); }
            }
            return gameDefinitions;
        }

        public static List<GameDefinition> getAllGameDefinitions()
        {
            List<GameDefinition> definitions = new List<GameDefinition>();
            definitions.Add(automobilista);
            definitions.Add(AMS2);
            definitions.Add(gameStockCar);
            definitions.Add(marcas);
            definitions.Add(ftruck);
            definitions.Add(arcaSimRacing);
            definitions.Add(pCars2);
            definitions.Add(pCars3);
            definitions.Add(pCars64Bit);
            definitions.Add(pCars32Bit);
            definitions.Add(raceRoom);
            definitions.Add(pCarsNetwork);

            // TODO: reinstate this when it actually works:
            // definitions.Add(pCars2Network);

            definitions.Add(rFactor1);
            definitions.Add(assetto64Bit);
            definitions.Add(assetto64BitRallyMode);
            definitions.Add(assetto32Bit);
            definitions.Add(assettoEvo);
            definitions.Add(assettoPro);
            definitions.Add(rfactor2_64bit);
            definitions.Add(lmu);
            definitions.Add(iracing);
            definitions.Add(acc);
            definitions.Add(f1_2018);
            definitions.Add(f1_2019);
            definitions.Add(f1_2020);
            definitions.Add(f1_2021);
            definitions.Add(f1_2022);
            definitions.Add(f1_2023);
            definitions.Add(rbr);
            definitions.Add(gtr2);
            definitions.Add(none);
            definitions.Add(dirt);
            definitions.Add(dirt2);
            return definitions;
        }
        public static List<GameDefinition> getAllAvailableGameDefinitions(Boolean includeAllSupportedGamesEntry)
        {
            List<GameDefinition> definitions = getAllGameDefinitions();
            if (includeAllSupportedGamesEntry) definitions.Add(any);
            return filterAvailableGames(definitions);
        }

        public static GameDefinition getGameDefinitionForFriendlyName(String friendlyName)
        {
            List<GameDefinition> definitions = getAllAvailableGameDefinitions(true);
            foreach (GameDefinition def in definitions)
            {
                if (def.friendlyName == friendlyName)
                {
                    return def;
                }
            }
            return null;
        }

        public static GameDefinition getGameDefinitionForCommandLineName(String commandLineName)
        {
            List<GameDefinition> definitions = getAllAvailableGameDefinitions(false);
            foreach (GameDefinition def in definitions)
            {
                if (def.commandLineName == commandLineName)
                {
                    return def;
                }
            }
            return null;
        }

        public static String[] getGameDefinitionFriendlyNames()
        {
            List<String> names = new List<String>();
            foreach (GameDefinition def in getAllAvailableGameDefinitions(false))
            {
                names.Add(def.friendlyName);
            }
            names.Sort();
            return names.ToArray();
        }

        public String lookupName;
        public GameEnum gameEnum;
        public String friendlyName;
        public String macroEditorName;
        public readonly CrewChief.RacingType racingType = CrewChief.RacingType.Undefined;
        public String processName;
        public String spotterName;
        public String gameStartCommandProperty;
        public String gameStartCommandOptionsProperty;
        public String gameStartEnabledProperty;
        public String gameInstallDirectory;  // Not the full path, only used by games that need to install plugins
        public String[] alternativeProcessNames;
        public String[] alternativeFilterNames;
        public Boolean allowsUserCreatedCars;
        public String commandLineName;

        public enum FuelMultiplierType
        {
            /// <summary>
            /// PCARS3 doesn't have fuel consumption
            /// </summary>
            None,
            
            /// <summary>
            /// Game doesn't have a fuel multiplier, use 1
            /// </summary>
            Fixed,
            
            /// <summary>
            /// rFactor 2 provides the multiplier in shared memory, show it
            /// </summary>
            SharedByGame,
            
            /// <summary>
            /// Game only provides whether fuel consumption is a factor but
            /// doesn't provide the multiplier (if any) so if it's not 0
            /// use the value provided by the user
            /// </summary>
            Variable
        }
        public FuelMultiplierType fuelMultiplierType;

        public GameDefinition(GameEnum gameEnum,
            String lookupName,
            String commandLineName,
            String processName,
            String spotterName,
            String gameStartCommandProperty,
            String gameStartCommandOptionsProperty,
            String gameStartEnabledProperty,
            Boolean allowsUserCreatedCars,
            String gameInstallDirectory,
            String macroEditorName,
            CrewChief.RacingType racingType,
            FuelMultiplierType fuelMultiplierType,
            String[] alternativeProcessNames,
            String[] approxFilterNames
        )
        {
            this.gameEnum = gameEnum;
            this.lookupName = lookupName;
            this.friendlyName = this.macroEditorName = macroEditorName;
            this.processName = processName;
            this.spotterName = spotterName;
            this.gameStartCommandProperty = gameStartCommandProperty;
            this.gameStartCommandOptionsProperty = gameStartCommandOptionsProperty;
            this.gameStartEnabledProperty = gameStartEnabledProperty;
            this.alternativeProcessNames = alternativeProcessNames;
            this.gameInstallDirectory = gameInstallDirectory;
            this.allowsUserCreatedCars = allowsUserCreatedCars;
            this.fuelMultiplierType = fuelMultiplierType;
            this.racingType = racingType;
            this.commandLineName = commandLineName == null ? gameEnum.ToString() : commandLineName;
            this.alternativeFilterNames = approxFilterNames;
        }
        /// <summary>
        /// ctor to initialise gameDefinition
        /// </summary>
        public GameDefinition()
        {
            racingType = CrewChief.RacingType.Undefined;
        }
        public bool HasAnyProcessNameAssociated()
        {
            return processName != null
                || (alternativeProcessNames != null && alternativeProcessNames.Length > 0);
        }
    }
}
