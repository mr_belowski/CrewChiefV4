﻿namespace F12022UdpNet
{
    public enum ValueVariationType : sbyte
    {
        Increased = 0,
        Decreased = 1,
        NoChange = 2,
    }
}