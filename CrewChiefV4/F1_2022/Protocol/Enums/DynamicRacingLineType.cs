﻿namespace F12022UdpNet
{
    public enum DynamicRacingLineType : byte
    {
        Line2D = 0,
        Line3D = 1,
    }
}