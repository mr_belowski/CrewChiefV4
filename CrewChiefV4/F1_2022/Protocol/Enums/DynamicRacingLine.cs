﻿namespace F12022UdpNet
{
    public enum DynamicRacingLine : byte
    {
        Off = 0,
        CornersOnly = 1,
        Full = 2,
    }
}