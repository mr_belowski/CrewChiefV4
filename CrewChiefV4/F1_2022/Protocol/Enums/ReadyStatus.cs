﻿namespace F12022UdpNet
{
    public enum ReadyStatus : byte
    {
        NotReady = 0,
        Ready = 1,
        Spectating = 2,
    }
}