﻿namespace F12022UdpNet
{
    public enum SpeedUnit : byte
    {
        MPH = 0,
        KPH = 1,
    }
}