﻿namespace F12022UdpNet
{
    public enum PitStatus : byte
    {
        None = 0,
        Pitting = 1,
        PitArea = 2
    }
}