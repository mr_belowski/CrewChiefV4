﻿namespace F12022UdpNet { 
    public enum Platform : byte
    {
        Steam = 1,
        Playstation = 3,
        Xbox = 4, 
        Origin = 6, 
        Unknown = 255
    }
}
