﻿namespace F12022UdpNet
{
    public enum FuelMix : byte
    {
        Lean = 0,
        Standard = 1,
        Rich = 2,
        Max = 3,
    }
}