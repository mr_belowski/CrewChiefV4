﻿namespace F12022UdpNet
{
    public enum Accuracy : byte
    {
        Perfect = 0,
        Approximate = 1,
    }
}