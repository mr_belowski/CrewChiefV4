﻿namespace F12022UdpNet
{
    public enum TyreCompound : byte
    {
        /** F1 Modern */
        C5 = 16,
        C4 = 17,
        C3 = 18,
        C2 = 19,
        C1 = 20,
        Inter = 7,
        Wet = 8,

        /** F1 Classic */
        ClassicDry = 9,
        ClassicWet = 10,

        /** F2 */
        F2SuperSoft = 11,
        F2Soft = 12,
        F2Medium = 13,
        F2Hard = 14,
        F2Wet = 15
    }
}