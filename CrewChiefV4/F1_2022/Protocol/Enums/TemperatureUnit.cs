﻿namespace F12022UdpNet
{
    public enum TemperatureUnit : byte
    {
        Celsius = 0,
        Fahrenheit = 1,
    }
}