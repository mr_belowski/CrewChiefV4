﻿namespace F12022UdpNet
{
    public enum TractionControlStatus : byte
    {
        Off = 0,
        Medium = 1,
        Full = 2,
    }
}