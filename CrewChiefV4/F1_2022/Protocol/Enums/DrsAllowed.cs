﻿namespace F12022UdpNet
{
    public enum DrsAllowed : byte
    {
        NotAllowed = 0,
        Allowed = 1,
    }
}