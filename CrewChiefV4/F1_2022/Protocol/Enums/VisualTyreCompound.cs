﻿namespace F12022UdpNet
{
    public enum VisualTyreCompound : byte
    {
        /** F1 Modern */
        Soft = 16,
        Medium = 17,
        Hard = 18,
        Inter = 7,
        Wet = 8,

        /** F1 Classic */
        ClassicDry = 9,
        ClassicWet = 10,

        /** F2 */
        F2Wet = 15,
        F2SuperSoft = 19,
        F2Soft = 20,
        F2Medium = 21,
        F2Hard = 22
    }
}