﻿namespace F12023UdpNet
{
    public enum DynamicRacingLine : byte
    {
        Off = 0,
        CornersOnly = 1,
        Full = 2,
    }
}