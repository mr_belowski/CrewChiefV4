﻿namespace F12023UdpNet
{
    public enum DynamicRacingLineType : byte
    {
        Line2D = 0,
        Line3D = 1,
    }
}