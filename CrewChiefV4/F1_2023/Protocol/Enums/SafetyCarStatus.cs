﻿namespace F12023UdpNet
{
    public enum SafetyCarStatus : byte
    {
        None = 0,
        Full = 1,
        Virtual = 2,
        Formation = 3,
    }
}