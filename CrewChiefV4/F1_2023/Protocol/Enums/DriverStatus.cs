﻿namespace F12023UdpNet
{
    public enum DriverStatus
    {
        InGarage = 0,
        FlyingLap = 1,
        InLap = 2,
        OutLap = 3,
        OnTrack = 4
    }
}
