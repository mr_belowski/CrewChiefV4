﻿namespace F12023UdpNet
{
    public static class EventStringCode
    {
        public const string SessionStart = "SSTA";
        public const string SessionEnd = "SEND";
        public const string FastestLap = "FTLP";
        public const string DriverRetires = "RTMT";
        public const string DRSEnabled = "DRSE";
        public const string DRSDisabled = "DRSD";
        public const string TeamMateInPits = "TMPT";
        public const string ChequeredFlag = "CHQF";
        public const string RaceWinner = "RCWN";
        public const string Penalty = "PENA";
        public const string SpeedTrap = "SPTP";
        public const string StartLights = "STLG";
        public const string LightsOut = "LGOT";
        public const string DriveThroughServed = "DTSV";
        public const string StopAndGoServed = "SGSV";
        public const string Flashback = "FLBK";
        public const string ButtonStatusChanged = "BUTN";
        public const string RedFlag = "RDFL";
        public const string Overtake = "OVTK";
    }
}