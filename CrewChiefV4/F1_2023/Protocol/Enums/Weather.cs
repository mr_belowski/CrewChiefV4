﻿namespace F12023UdpNet
{
    public enum Weather : byte
    {
        Clear = 0,
        LightCloud = 1,
        Overcase = 2,
        LightRain = 3,
        HeavyRain = 4,
        Storm = 5
    }
}