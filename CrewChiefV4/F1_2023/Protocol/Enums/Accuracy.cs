﻿namespace F12023UdpNet
{
    public enum Accuracy : byte
    {
        Perfect = 0,
        Approximate = 1,
    }
}