﻿namespace F12023UdpNet
{
    public enum GearboxAssist
    {
        Manual = 1,
        ManualWithSuggestedGear = 2,
        Auto = 3,
    }
}
