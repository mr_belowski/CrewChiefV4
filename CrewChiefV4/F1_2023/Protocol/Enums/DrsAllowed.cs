﻿namespace F12023UdpNet
{
    public enum DrsAllowed : byte
    {
        NotAllowed = 0,
        Allowed = 1,
    }
}