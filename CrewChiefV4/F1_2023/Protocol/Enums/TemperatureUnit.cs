﻿namespace F12023UdpNet
{
    public enum TemperatureUnit : byte
    {
        Celsius = 0,
        Fahrenheit = 1,
    }
}