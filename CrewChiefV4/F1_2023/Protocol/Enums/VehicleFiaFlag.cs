﻿namespace F12023UdpNet
{
    public enum VehicleFiaFlag : sbyte
    {
        Unknown = -1,
        None = 0,
        Green = 1,
        Blue = 2,
        Yellow = 3,
    }
}