﻿namespace F12023UdpNet
{
    public enum ErsDeployMode : byte
    {
        None = 0,
        Medium = 1,
        Hotlap = 2,
        Overtake = 3,
    }
}