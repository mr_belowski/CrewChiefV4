﻿namespace F12023UdpNet
{
    public enum FuelMix : byte
    {
        Lean = 0,
        Standard = 1,
        Rich = 2,
        Max = 3,
    }
}