﻿namespace F12023UdpNet
{
    public enum ValueVariationType : sbyte
    {
        Increased = 0,
        Decreased = 1,
        NoChange = 2,
    }
}