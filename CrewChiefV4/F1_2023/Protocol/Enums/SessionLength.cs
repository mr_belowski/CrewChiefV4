﻿namespace F12023UdpNet
{
    public enum SessionLength : byte
    {
        None = 0,
        VeryShort = 2,
        Short = 3,
        Medium = 4,
        MediumLong = 5,
        Long = 6,
        Full = 7,
    }
}