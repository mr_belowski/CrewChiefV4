﻿namespace F12023UdpNet
{
    public enum TractionControlStatus : byte
    {
        Off = 0,
        Medium = 1,
        Full = 2,
    }
}