﻿namespace F12023UdpNet
{
    public enum BrakingAssist : byte
    {
        Off = 0,
        Low = 1,
        Medium = 2,
        High = 3,
    }
}