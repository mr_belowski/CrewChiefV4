﻿namespace F12023UdpNet
{
    public enum Formula : byte
    {
        F1Modern = 0,
        F1Classic = 1,
        F2 = 2,
        F1Generic = 3,
        Beta = 4,
        Supercars = 5,
        Esports = 6,
        F22021 = 7,
    }
}