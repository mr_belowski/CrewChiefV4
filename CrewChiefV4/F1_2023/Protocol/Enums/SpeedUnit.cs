﻿namespace F12023UdpNet
{
    public enum SpeedUnit : byte
    {
        MPH = 0,
        KPH = 1,
    }
}