﻿#if PIT_MANAGER_DEBUG
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.InteropServices;
using CrewChiefV4.UserInterface.VMs;

using PitMenuAPI;

using static PitMenuAPI.PitMenuAbstractionLayer;

namespace CrewChiefV4.UserInterface.Models
{
    internal class PitMenuDebug
    {
        public enum DirectionButton
        {
            UP,
            DOWN,
            LEFT,
            RIGHT
        }

        private readonly PitMenuDebug_VM viewModel;
        private readonly PitMenu pm;
        static readonly PitMenuController pmc = new PitMenuController();
        private static PitMenuController.PitMenuContents menuDict;
        public PitMenuDebug(PitMenuDebug_VM _viewModel)
        {
            viewModel = _viewModel;
            Log.setLogLevel(Log.LogType.Verbose);
            Serilog.Log.Information("Crew Chief logging via Serilog.");
            pm = new PitMenu();
            WindowsControl.bringGameToForeground();
            pm.startUsingPitMenu();
            menuDict = pmc.GetMenuContents();
            viewModel.WriteMenu(menuDict);
            FillMenuChoices();
            string pitCategory = pm.GetCategory();
            viewModel.CheckmarkPitCategory(pitCategory);
        }

        public void MenuButton(DirectionButton directionButton)
        {
            WindowsControl.bringGameToForeground();
            //pm.startUsingPitMenu();
            switch (directionButton)
            {
                case DirectionButton.UP:
                    pm.CategoryUp();
                    break;
                case DirectionButton.DOWN:
                    pm.CategoryDown();
                    break;
                case DirectionButton.LEFT:
                    pm.ChoiceDec();
                    break;
                case DirectionButton.RIGHT:
                    pm.ChoiceInc();
                    break;
            }
        }

        public void SwitchMFD(string mfd)
        {
            WindowsControl.bringGameToForeground();
            pm.switchMFD(mfd);
            viewModel.SetMfdDropdown(mfd);
        }
        public void SelectPitCategory(string pitCategory)
        {
            WindowsControl.bringGameToForeground();
            pm.SetCategory(pitCategory);
            pitCategory = pm.GetCategory();
            viewModel.CheckmarkPitCategory(pitCategory);
            viewModel.SetMfdDropdown("MFDB");
        }

        public void FillMenuChoices()
        {
            List<string> menuChoices = new List<string>();
            var currentCategory = pm.GetCategory();
            foreach (var category in menuDict.items)
            {
                SelectPitCategory(category.category);
                menuChoices.Add(pm.GetChoice());
            }

            SelectPitCategory(currentCategory);
            viewModel.SetChoices(menuChoices);
        }
        public void SetFuel(string litres)
        {
            int fuel;
            //SelectPitCategory("FUEL:");
            if (int.TryParse(litres, out fuel))
            {
                WindowsControl.bringGameToForeground();
                pmc.SetFuelLevel(fuel);
                FillMenuChoices();
            }
            else
            {
                Log.Error($"Can't parse fuel level '{litres}'");
            }
        }
    }
    public class WindowsControl
    {
        [DllImport("user32.dll")]
        public static extern bool SetForegroundWindow(IntPtr hWnd);
        [DllImport("user32.dll")]
        private static extern IntPtr GetForegroundWindow();

        private static readonly List<string> processNames = new List<string>{"rFactor2","Le Mans Ultimate"};
        private static Process[] matchingProcesses = Process.GetProcesses();
        private static string processName;

        public static void bringGameToForeground()
        {
            IntPtr currentForgroundWindow = GetForegroundWindow();
            SetGameProcessAsForeground(currentForgroundWindow);
        }
        private static bool SetGameProcessAsForeground(IntPtr currentForgroundWindow)
        {
            foreach (var gameProcess in matchingProcesses)
            {
                if (processNames.Contains(gameProcess.ProcessName) &&
                    gameProcess.MainWindowHandle != (IntPtr)0 &&
                    gameProcess.MainWindowHandle != currentForgroundWindow)
                {
                    if (SetForegroundWindow(gameProcess.MainWindowHandle))
                    {
                        return true;
                    }
                    else
                    {
                        Console.WriteLine($"Couldn't set {processName} to be the current window");
                        throw new System.InvalidOperationException($"Couldn't set {processName} to be the current window");
                    }
                }
            }
            return false;
        }
    }

}
#endif
