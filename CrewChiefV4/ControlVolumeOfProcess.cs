﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using NAudio.CoreAudioApi;
using NAudio.CoreAudioApi.Interfaces;
using Newtonsoft.Json;
using WebSocketSharp;

namespace CrewChiefV4
{
    #region Controlling the volume of a process

    public class ControlVolumeOfProcess
    {
        private AudioSessionControl _session;
        internal string currentGame;

        /// <summary>
        ///     Change the volume of a Windows process
        /// </summary>
        /// <param name="processName">the exact, case-sensitive name of the process</param>
        public ControlVolumeOfProcess(string processName)
        {
            Init(processName);
        }

        public ControlVolumeOfProcess()
        {
            currentGame = CrewChief.gameDefinition.processName;
            if (!currentGame.IsNullOrEmpty())
            {
                Init(currentGame);
            }
        }

        /// <summary>
        ///     The Process class used in volume control, may be useful.
        /// </summary>
        internal Process TheProcess { get; set; }
        internal float originalVolume { get; private set; }

        /// <summary>
        ///     Check if the process is using an audio session.
        ///     Set _session if it is.
        /// </summary>
        private void Init(string processName)
        {
            TheProcess = null;
            _session = null;
            var MMDE = new MMDeviceEnumerator();
            MMDeviceCollection devCol = MMDE.EnumerateAudioEndPoints(DataFlow.All, DeviceState.Active);
            Log.Debug($"processName {processName}");
            processName = processName.Trim().ToLower();

            foreach (MMDevice dev in devCol)
            {
                SessionCollection sessions = dev.AudioSessionManager.Sessions;
                for (int i = 0; i < sessions.Count; i++)
                {
                    if (sessions[i].State == AudioSessionState.AudioSessionStateActive)
                    {
                        var process = Process.GetProcessById((int)sessions[i].GetProcessID);
                        Log.Verbose(process.ProcessName);
                        if (process.ProcessName.ToLower().Equals(processName)) // && !String.IsNullOrEmpty(process.MainWindowTitle)) Discord window title is blank
                        {
                            TheProcess = process;
                            _session = sessions[i];
                            return;
                        }
                    }
                }
            }
        }

        /// <summary>
        ///     Change the volume of a Windows process
        /// </summary>
        /// <param name="duckPercent"> -ve to reduce volume, +ve to increase it</param>
        internal float DuckVolume(int duckPercent)
        {
            float newVolume = 0;
            if (_session != null)
            {
                SimpleAudioVolume vol = _session.SimpleAudioVolume;
                vol.Volume = Math.Max(vol.Volume, 0.01f); // make sure it's not 0
                newVolume = Math.Min(vol.Volume * ((float)(100 + duckPercent) / 100), 1.0f);
                vol.Volume = newVolume;
                newVolume = vol.Volume;
            }
            return newVolume;
        }

        internal float GetVolume()
        {
            return _session != null ? _session.SimpleAudioVolume.Volume : -1.0f;
        }

        internal bool SetVolume(float volume)
        {
            if (_session != null)
            {
                originalVolume = _session.SimpleAudioVolume.Volume; // ?? This has already been set
                volume = Math.Max(volume, 0.0f);
                volume = Math.Min(volume, 1.0f);
                _session.SimpleAudioVolume.Volume = volume;
            }
            return _session != null;
        }
    }

    #endregion Controlling the volume of a process

    #region Controlling the volume of the current game or VOIP app

    internal class GameOrVoipVolume
    {
        private readonly string _currentGame;
        private readonly bool _gameRunning;
        private readonly bool _voipRunning;
        private readonly Volumes _volumes;
        internal ControlVolumeOfProcess GameProcess;
        internal ControlVolumeOfProcess VoipProcess;

        internal GameOrVoipVolume(bool voip)
        {
            if (!VolumeControlEnabled && !AudioDuckingEnabled && !AudioDuckingWhileTalkingToCcEnabled)
            {
                return;
            }
            try
            {
                string json = File.ReadAllText(DataFiles.game_volumes);
                _volumes = JsonConvert.DeserializeObject<Volumes>(json);
                Log.Debug($"DataFiles.game_volumes {DataFiles.game_volumes}");
            }
            catch
            {
                Log.Error($"Failed to read {DataFiles.game_volumes}");
                _volumes = new Volumes();
                _volumes.GameVolumes = new Dictionary<string, float>();
                foreach (GameDefinition game in GameDefinition.getAllGameDefinitions())
                {
                    if (game.processName != null)
                    {
                        _volumes.GameVolumes[game.processName] = 1.0f;
                    }
                }
                _volumes.VoipProcessName = "Discord";
                _volumes.GameVolumes[_volumes.VoipProcessName] = 1.0f;
                _volumes.DuckPercentage = 80;
                _volumes.DuckPercentageTalkingToCc = 80;
                _volumes.VolumeIncrementPercentage = 10;
                DataFiles.WriteJsonFile(DataFiles.game_volumes, _volumes);
            }
            if (_volumes.DuckPercentageTalkingToCc == 0)
            {
                _volumes.DuckPercentageTalkingToCc = 80;
                Log.Warning("New field DuckPercentageTalkingToCc initialised");
                DataFiles.WriteJsonFile(DataFiles.game_volumes, _volumes);
            }
            if (!voip)
            {
                GameProcess = new ControlVolumeOfProcess();
                if (GameProcess.TheProcess != null)
                {
                    _currentGame = GameProcess.currentGame;
                    GameProcess.SetVolume(_volumes.GameVolumes[GameProcess.currentGame]);
                }
                else
                {
                    Log.Commentary($"{GameProcess.currentGame} is not running");
                }
                _gameRunning = GameProcess.TheProcess != null;
            }
            else if (VolumeControlEnabled)
            {
                Log.Debug($"_volumes.VoipProcessName {_volumes.VoipProcessName}");
                VoipProcess = new ControlVolumeOfProcess(_volumes.VoipProcessName);
                if (VoipProcess.TheProcess != null)
                {
                    VoipProcess.SetVolume(_volumes.GameVolumes[_volumes.VoipProcessName]);
                }
                else
                {
                    Log.Commentary($"{_volumes.VoipProcessName} is not running");
                }
                _voipRunning = VoipProcess.TheProcess != null;
            }
        }

        public static bool VolumeControlEnabled { get; internal set; } = UserSettings.GetUserSettings().getBoolean("enable_volume_control");
        public static bool AudioDuckingEnabled { get; internal set; } = UserSettings.GetUserSettings().getBoolean("enable_audio_ducking");
        public static bool AudioDuckingWhileTalkingToCcEnabled { get; internal set; } = UserSettings.GetUserSettings().getBoolean("enable_audio_ducking_while_talking_to_cc");

        [JsonObject]
        private class Volumes
        {
            public int DuckPercentage; // how much a game's audio is reduced while CC is playing sounds
            public int DuckPercentageTalkingToCc; // how much a game's audio is reduced while talking to CC
            public Dictionary<string, float> GameVolumes; // the volume CC sets each game to
            public string VoipProcessName; // the app used for VOIP
            public int VolumeIncrementPercentage; // how big a step is used by volume up/down commands
        }

        #region game

        private static GameOrVoipVolume gameVolumeObj;

        private static bool CurrentGame()
        {
            if (gameVolumeObj == null && CrewChief.gameDefinition.processName != null)
            {
                gameVolumeObj = new GameOrVoipVolume(false);
            }
            if (gameVolumeObj != null && !gameVolumeObj._gameRunning)
            {
                gameVolumeObj = null;
            }
            return gameVolumeObj != null;
        }

        private static GameOrVoipVolume voipVolumeObj;

        private static bool ControlVolumeOfCurrentVoipInstance()
        {
            if (voipVolumeObj == null)
            {
                voipVolumeObj = new GameOrVoipVolume(true);
            }
            if (!voipVolumeObj._voipRunning)
            {
                voipVolumeObj = null;
            }
            return voipVolumeObj != null;
        }

        /// <summary>
        ///     Reduce game volume while CC is playing a sound
        /// </summary>
        public static void DuckGameAudio()
        {
            if (CurrentGame())
            {
                gameVolumeObj.GameProcess.DuckVolume(-gameVolumeObj._volumes.DuckPercentage);
            }
        }

        /// <summary>
        ///     Reduce game volume while talking to CC
        /// </summary>
        public static void DuckGameAudioTalkingToCc()
        {
            if (CurrentGame())
            {
                gameVolumeObj.GameProcess.DuckVolume(-gameVolumeObj._volumes.DuckPercentageTalkingToCc);
            }
        }

        /// <summary>
        ///     Restore the volume of the game
        /// </summary>
        public static void UnDuckGameAudio()
        {
            if (CurrentGame())
            {
                gameVolumeObj.GameProcess.SetVolume(gameVolumeObj._volumes.GameVolumes[gameVolumeObj._currentGame]);
            }
        }

        /// <summary>
        ///     Game volume louder. (CC will set it to this next time the game starts)
        /// </summary>
        public static void IncreaseGameVolume()
        {
            if (VolumeControlEnabled && CurrentGame())
            {
                gameVolumeObj._volumes.GameVolumes[gameVolumeObj._currentGame] =
                    Math.Min(gameVolumeObj._volumes.GameVolumes[gameVolumeObj._currentGame] +
                             (float)gameVolumeObj._volumes.VolumeIncrementPercentage / 100, 1.0f);
                gameVolumeObj.SetGameVolume();
            }
        }

        /// <summary>
        ///     Game volume quieter. (CC will set it to this next time the game starts)
        /// </summary>
        public static void DecreaseGameVolume()
        {
            if (VolumeControlEnabled && CurrentGame())
            {
                gameVolumeObj._volumes.GameVolumes[gameVolumeObj._currentGame] =
                    Math.Max(gameVolumeObj._volumes.GameVolumes[gameVolumeObj._currentGame] -
                             (float)gameVolumeObj._volumes.VolumeIncrementPercentage / 100, 0.0f);
                gameVolumeObj.SetGameVolume();
            }
        }

        private void SetGameVolume()
        {
            if (VolumeControlEnabled && CurrentGame())
            {
                gameVolumeObj.GameProcess.SetVolume(gameVolumeObj._volumes.GameVolumes[_currentGame]);
                DataFiles.WriteJsonFile(DataFiles.game_volumes, gameVolumeObj._volumes);
                Log.Commentary($"{_currentGame} volume set to {(int)(gameVolumeObj._volumes.GameVolumes[_currentGame] * 100)}%");
            }
        }

        #endregion game

        #region VOIP

        /// <summary>
        ///     VOIP volume louder. (CC will set it to this next time the game starts)
        /// </summary>
        public static void IncreaseVoipVolume()
        {
            if (VolumeControlEnabled && ControlVolumeOfCurrentVoipInstance())
            {
                voipVolumeObj._volumes.GameVolumes[voipVolumeObj._volumes.VoipProcessName] =
                    Math.Min(voipVolumeObj._volumes.GameVolumes[voipVolumeObj._volumes.VoipProcessName] +
                             (float)voipVolumeObj._volumes.VolumeIncrementPercentage / 100, 1.0f);
                voipVolumeObj.SetVoipVolume();
            }
        }

        /// <summary>
        ///     VOIP volume quieter. (CC will set it to this next time the game starts)
        /// </summary>
        public static void DecreaseVoipVolume()
        {
            if (VolumeControlEnabled && ControlVolumeOfCurrentVoipInstance())
            {
                voipVolumeObj._volumes.GameVolumes[voipVolumeObj._volumes.VoipProcessName] =
                    Math.Max(voipVolumeObj._volumes.GameVolumes[voipVolumeObj._volumes.VoipProcessName] -
                             (float)voipVolumeObj._volumes.VolumeIncrementPercentage / 100, 0.0f);
                voipVolumeObj.SetVoipVolume();
            }
        }

        private void SetVoipVolume()
        {
            voipVolumeObj.VoipProcess.SetVolume(voipVolumeObj._volumes.GameVolumes[voipVolumeObj._volumes.VoipProcessName]);
            DataFiles.WriteJsonFile(DataFiles.game_volumes, voipVolumeObj._volumes);
            Log.Commentary($"Voip volume set to {(int)(voipVolumeObj._volumes.GameVolumes[voipVolumeObj._volumes.VoipProcessName] * 100)}%");
        }

        #endregion VOIP

        /// <summary>
        /// Set the game and VOIP volumes to the levels previously selected in CC
        /// </summary>
        public static void SetVolumes()
        {
            if (VolumeControlEnabled && CurrentGame())
            {
                gameVolumeObj.SetGameVolume();
            }
            if (VolumeControlEnabled && ControlVolumeOfCurrentVoipInstance())
            {
                voipVolumeObj.SetVoipVolume();
            }
        }
        /// <summary>
        /// Set the game and VOIP volumes back to the original levels
        /// </summary>
        public static void RestoreVolumes()
        {
            if (VolumeControlEnabled && CurrentGame())
            {
                gameVolumeObj.GameProcess.SetVolume(1.0f); // tbd gameVolumeObj.GameProcess.originalVolume);
                Log.Commentary("Game volume mixer level set back to 100%");
            }
            if (VolumeControlEnabled && ControlVolumeOfCurrentVoipInstance())
            {
                voipVolumeObj.VoipProcess.SetVolume(1.0f); // tbd voipVolumeObj.VoipProcess.originalVolume);
                Log.Commentary($"{voipVolumeObj.VoipProcess.TheProcess.ProcessName} volume mixer level set back to 100%");
            }
        }
    }

    #endregion Controlling the volume of the current game or VOIP app
}