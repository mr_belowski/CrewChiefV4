﻿using System;

namespace CrewChiefV4
{
    [Flags]
    public enum GameEnum
    {
        ACC                 = 1 << 0,
        AMS2                = 1 << 1,
        AMS2_NETWORK        = 1 << 2,
        ASSETTO_32BIT       = 1 << 3,
        ASSETTO_64BIT       = 1 << 4,
        ASSETTO_64BIT_RALLY = 1 << 5,
        ASSETTO_EVO         = 1 << 6,
        ASSETTO_PRO         = 1 << 7,
        DIRT                = 1 << 8,
        DIRT_2              = 1 << 9,
        F1_2018             = 1 << 10,
        F1_2019             = 1 << 11,
        F1_2020             = 1 << 12,
        F1_2021             = 1 << 13,
        F1_2022             = 1 << 14,
        F1_2023             = 1 << 15,
        GTR2                = 1 << 16,
        IRACING             = 1 << 17,
        LMU                 = 1 << 18,
        PCARS2              = 1 << 19,
        PCARS2_NETWORK      = 1 << 20,
        PCARS3              = 1 << 21,
        PCARS_32BIT         = 1 << 22,
        PCARS_64BIT         = 1 << 23,
        PCARS_NETWORK       = 1 << 24,
        RACE_ROOM           = 1 << 25,
        RBR                 = 1 << 26,
        RF1                 = 1 << 27,
        RF2_64BIT           = 1 << 28,

        UNKNOWN             = 1 << 29,
        NONE                = 1 << 30, /* this allows CC macros to run when an unsupported game is being played, it's selectable from the Games list */
        ANY                 = 1 << 31  /* this allows CC macros to be defined that apply to all supported games, it's only selectable from the macro UI */
    }
    public static class Game
    {
        public static GameEnum game
        { get; set; }

        public static bool ACC                 => game == GameEnum.ACC;
        public static bool AMS2                => game == GameEnum.AMS2;
        public static bool AMS2_NETWORK        => game == GameEnum.AMS2_NETWORK;
        public static bool ASSETTO_32BIT       => game == GameEnum.ASSETTO_32BIT;
        public static bool ASSETTO_64BIT       => game == GameEnum.ASSETTO_64BIT;
        public static bool ASSETTO_64BIT_RALLY => game == GameEnum.ASSETTO_64BIT_RALLY;
        public static bool ASSETTO_PRO         => game == GameEnum.ASSETTO_PRO;
        public static bool DIRT                => game == GameEnum.DIRT;
        public static bool DIRT_2              => game == GameEnum.DIRT_2;
        public static bool F1_2018             => game == GameEnum.F1_2018;
        public static bool F1_2019             => game == GameEnum.F1_2019;
        public static bool F1_2020             => game == GameEnum.F1_2020;
        public static bool F1_2021             => game == GameEnum.F1_2021;
        public static bool F1_2022             => game == GameEnum.F1_2022;
        public static bool F1_2023             => game == GameEnum.F1_2023;
        public static bool GTR2                => game == GameEnum.GTR2;
        public static bool IRACING             => game == GameEnum.IRACING;
        public static bool LMU                 => game == GameEnum.LMU;
        public static bool PCARS2              => game == GameEnum.PCARS2;
        public static bool PCARS2_NETWORK      => game == GameEnum.PCARS2_NETWORK;
        public static bool PCARS3              => game == GameEnum.PCARS3;
        public static bool PCARS_32BIT         => game == GameEnum.PCARS_32BIT;
        public static bool PCARS_64BIT         => game == GameEnum.PCARS_64BIT;
        public static bool PCARS_NETWORK       => game == GameEnum.PCARS_NETWORK;
        public static bool RACE_ROOM           => game == GameEnum.RACE_ROOM;
        public static bool RBR                 => game == GameEnum.RBR;
        public static bool RF1                 => game == GameEnum.RF1;
        public static bool RF2_64BIT           => game == GameEnum.RF2_64BIT;
        public static bool UNKNOWN             => game == GameEnum.UNKNOWN;
        public static bool NONE                => game == GameEnum.NONE;
        public static bool ANY                 => game == GameEnum.ANY;

        public static bool ASSETTO1            => (game & (GameEnum.ASSETTO_32BIT | GameEnum.ASSETTO_64BIT | GameEnum.ASSETTO_PRO)) != 0;
        public static bool PCARS1              => (game & (GameEnum.PCARS_32BIT | GameEnum.PCARS_64BIT | GameEnum.PCARS_NETWORK)) != 0;
        public static bool RF2_LMU             => (game & (GameEnum.RF2_64BIT | GameEnum.LMU)) != 0;
        public static bool F1_20S              => (game & (GameEnum.F1_2018 | GameEnum.F1_2019 | GameEnum.F1_2020 | GameEnum.F1_2021 | GameEnum.F1_2022 | GameEnum.F1_2023)) != 0;
    }
}
