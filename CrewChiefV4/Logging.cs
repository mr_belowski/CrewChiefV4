﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

[assembly: System.Runtime.CompilerServices.InternalsVisibleTo("UnitTest")]

namespace CrewChiefV4
{
    /// <summary>
    /// Class to encapsulate Console.Write, with LogType added to each call.<br/>
    /// Each type can be turned on or off.<br/>
    /// Each log has its type prepended before it is written to the console.
    /// </summary>
    static class Log
    {
        [Flags]
        internal enum LogType
        {
            FatalError = 1 << 0,
            Error = 1 << 1,
            Warning = 1 << 2,
            Commentary = 1 << 3,
            Subtitle = 1 << 4,  // All here and up show in release builds
            Info = 1 << 5,
            Fuel = 1 << 6,
            DebugError = 1 << 7,
            Debug = 1 << 8,     // All here and up show in debug builds, shown if log_type_debug is set
            Verbose = 1 << 9,   // Shown if log_type_verbose is set
            Exception = 1 << 10
        };
        // Initialise from the most inclusive (Verbose) to the least (Subtitle).
        private static LogType _logMask = 
            UserSettings.GetUserSettings().getBoolean("log_type_verbose") ? setLogLevel(LogType.Verbose) :
                (UserSettings.GetUserSettings().getBoolean("log_type_debug") ? setLogLevel(LogType.Debug) :
                    setLogLevel(LogType.Subtitle));
        private static readonly Dictionary<LogType, string> logPrefixes = new
            Dictionary<LogType, string>
        {
            { LogType.FatalError, "FATAL ERROR: " },
            { LogType.Error     , "ERROR: " },
            { LogType.Warning   , "Warn: " },
            { LogType.Commentary, "Cmnt: " },
            { LogType.Subtitle,   "Subt: " },
            { LogType.Info      , "Info: " },
            { LogType.Fuel      , "Fuel: " },
            { LogType.DebugError, "DEBUG ERROR: " },
            { LogType.Debug     , "Dbug: " },
            { LogType.Verbose   , "Verb: " },
            { LogType.Exception , "EXCEPTION: " }
        };
        /// <summary>
        /// Set the log mask so all logs up to "logType" are shown
        /// </summary>
        /// <param name="logType"> log level</param>
        /// <returns></returns>
        public static LogType setLogLevel(LogType logType)
        {
            _logMask = 0;
            while (logType != 0)
            {
                _logMask |= logType;
                logType = (LogType)((int)logType / 2);
            }
            return _logMask;
        }

        public static bool logDebug
        {
            get => (_logMask & LogType.Debug) != 0;
            set
            {
                UserSettings.GetUserSettings().setProperty("log_type_debug", value);
                if (value)
                {
                    _logMask &= ~LogType.Debug;
                }
                else
                {
                    _logMask |= LogType.Debug;
                }
                UserSettings.GetUserSettings().saveUserSettings();
            }
        }
        public static bool logVerbose
        {
            get => (_logMask & LogType.Verbose) != 0;
            set
            {
                UserSettings.GetUserSettings().setProperty("log_type_verbose", value);
                if (value)
                {
                    _logMask |= LogType.Verbose;
                }
                else
                {
                    _logMask &= ~LogType.Verbose;
                }
                UserSettings.GetUserSettings().saveUserSettings();
            }
        }
        public static bool logFuel
        {
            get => (_logMask & LogType.Fuel) != 0;
            set
            {
                UserSettings.GetUserSettings().setProperty("log_type_fuel", value);
                if (value)
                {
                    _logMask |= LogType.Fuel;
                }
                else
                {
                    _logMask &= ~LogType.Fuel;
                }
                UserSettings.GetUserSettings().saveUserSettings();
            }
        }

        public static LogType LogMask
        {
            get
            {
                return _logMask;
            }
            /// <summary>
            /// Set the log mask so all logs matching "logMask" are shown
            /// Can set individual types of log, for example
            ///   setLogMask(LogType.Error | LogType.Subtitle);
            /// </summary>
            /// <param name="logMask"> log mask</param>
            /// <returns></returns>
            set
            {
                _logMask = value;
                CrewChief.Debug.LoggingDebug = (_logMask & LogType.Debug) != 0;
            }
        }

        public delegate void LogSink(string log);
        public static LogSink logSink { get; set; } = Console.WriteLine;

        /// <summary>
        /// We ask users to send log files so replace c:\users\ NAME \My Documents with MY DOCUMENTS
        /// </summary>
        /// <param name="filePath"></param>
        /// <returns></returns>
        public static string AnonymisePath(string filePath)
        {
            return filePath.Replace(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), "MY DOCUMENTS");
        }
        /// <summary>
        /// Write "log" to Console if logType is enabled
        /// "log" has its type prepended before it is written.
        /// </summary>
        /// <param name="logType"></param>
        /// <param name="log"></param>
        internal static void _Log(LogType logType, string log)
        {
            log = AnonymisePath(log);
            if ((logType & _logMask) != 0)
            {
                if (logType == LogType.FatalError)
                {
                    logSink(new string('*', (logPrefixes[logType] + log).Length));
                    logSink(logPrefixes[logType] + log);
                    logSink(new string('*', (logPrefixes[logType] + log).Length));
                }
                else
                {
                    logSink(logPrefixes[logType] + log);
                }
            }
            if ((logType & (LogType.Error | LogType.FatalError | LogType.Exception)) != 0)
            {
                // Also write to the error log file
                try
                {
                    const string divider = "------------------";
                    string path = Path.Combine(DataFiles.UserLogsFolder, "ErrorLog.txt");
                    string currentErrors = File.Exists(path) ? File.ReadAllText(path) : "";
                    string timeStamp = DateTime.Now.ToString("yyyy-MM-dd HH:mm");
                    if (currentErrors.Contains(log))
                    {
                        File.AppendAllText(path, $"\n{timeStamp}: ");
                        File.AppendAllText(path, $"Repeat of {logPrefixes[logType]}  {log.Split('\n')[0]}");
                        Log.Commentary($"Duplicate {logType.ToString()} log added to {AnonymisePath(path)}");
                    }
                    else
                    {
                        List<string> errorReport = new List<string> { divider };
                        errorReport.Add($"{timeStamp}");
                        errorReport.AddRange(MainWindow.ErrorContext(path, 10));
                        errorReport.Add(logPrefixes[logType] + log);
                        errorReport.Add(divider);
                        File.AppendAllLines(path, errorReport);
                        Log.Commentary($"{logType.ToString()} log added to {AnonymisePath(path)}");
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                }
            }
        }
        /// <summary>
        /// Write "log" with object to Console if LogType is enabled
        /// Example:
        ///   logLog(LogType.Warning, "Warning log {0}", 1.5f);
        /// </summary>
        /// <param name="logType"></param>
        /// <param name="log">format string containing {0}</param>
        /// <param name="arg0">the object to replace {0}</param>
        internal static void _Log(LogType logType, string log, object arg0)
        {
            _Log(logType, String.Format(log, arg0));
        }
        internal static void _Log(LogType logType, string log, object arg0, object arg1)
        {
            _Log(logType, String.Format(log, arg0, arg1));
        }
        internal static void _Log(LogType logType, string log, object arg0, object arg1, object arg2)
        {
            _Log(logType, String.Format(log, arg0, arg1, arg2));
        }
        internal static void _Log(LogType logType, string log, object arg0, object arg1, object arg2, object arg3)
        {
            _Log(logType, String.Format(log, arg0, arg1, arg2, arg3));
        }
        internal static void _Log(LogType logType, string log, object arg0, object arg1, object arg2, object arg3, object arg4, object arg5)
        {
            _Log(logType, String.Format(log, arg0, arg1, arg2, arg3, arg4, arg5));
        }

        #region Shorthand calls
        /// <summary>
        /// Write "log" to Console if logType.FatalError is enabled
        /// </summary>
        /// <param name="log"></param>
        public static void Fatal(string log)
        {
            _Log(LogType.FatalError, log);
        }
        /// <summary>
        /// Write "log" to Console if logType.Error is enabled
        /// </summary>
        /// <param name="log"></param>
        public static void Error(string log)
        {
            _Log(LogType.Error, log);
        }
        /// <summary>
        /// Write "log" to Console if logType.Warning is enabled
        /// </summary>
        /// <param name="log"></param>
        public static void Warning(string log)
        {
            _Log(LogType.Warning, log);
        }
        public static void Warning(string log, object arg1, object arg2)
        {
            _Log(LogType.Warning, log, arg1, arg2);
        }
        /// <summary>
        /// Write "log" to Console if logType.Commentary is enabled
        /// </summary>
        /// <param name="log"></param>
        public static void Commentary(string log)
        {
            _Log(LogType.Commentary, log);
        }
        public static void Commentary(string log, object arg1)
        {
            _Log(LogType.Commentary, log, arg1);
        }
        public static void Commentary(string log, object arg1, object arg2)
        {
            _Log(LogType.Commentary, log, arg1, arg2);
        }
        public static void Commentary(string log, object arg1, object arg2, object arg3)
        {
            _Log(LogType.Commentary, log, arg1, arg2, arg3);
        }
        public static void Commentary(string log, object arg1, object arg2, object arg3, object arg4)
        {
            _Log(LogType.Commentary, log, arg1, arg2, arg3, arg4);
        }
        public static void Commentary(string log, object arg1, object arg2, object arg3, object arg4, object arg5, object arg6)
        {
            _Log(LogType.Commentary, log, arg1, arg2, arg3, arg4, arg5, arg6);
        }
        /// <summary>
        /// Write "log" to Console if logType.Subtitle is enabled
        /// </summary>
        /// <param name="log"></param>
        public static void Subtitle(string log)
        {
            _Log(LogType.Subtitle, log);
        }
        /// <summary>
        /// Write "log" to Console if logType.Info is enabled
        /// </summary>
        /// <param name="log"></param>
        public static void Info(string log)
        {
            _Log(LogType.Info, log);
        }
        /// <summary>
        /// Write "log" to Console if logType.Fuel is enabled
        /// </summary>
        /// <param name="log"></param>
        public static void Fuel(string log)
        {
            _Log(LogType.Fuel, log);
        }
        /// <summary>
        /// Write "log" to Console if logType.DebugError is enabled, cf. Debug.Assert
        /// </summary>
        /// <param name="log"></param>
        public static void DebugError(string log)
        {
            _Log(LogType.DebugError, log);
        }
        /// <summary>
        /// Write "log" to Console if logType.Debug is enabled
        /// </summary>
        /// <param name="log"></param>
        public static void Debug(string log)
        {
            _Log(LogType.Debug, log);
        }
        /// <summary>
        /// Write "log" to Console if logType.Verbose is enabled
        /// </summary>
        /// <param name="log"></param>
        public static void Verbose(string log)
        {
            _Log(LogType.Verbose, log);
        }
        /// <summary>
        /// Write Exception details to Console if logType.Exception is enabled
        /// Precede with log if the arg is included
        /// </summary>
        /// <param name="e">Exception</param>
        /// <param name="log">Optional text</param>
        public static void Exception(Exception e, string log="")
        {
            if (!string.IsNullOrEmpty(log))
            {
                log += Environment.NewLine;
            }
            _Log(LogType.Exception, log + e.Message + Environment.NewLine + e.StackTrace);
        }
        #endregion Shorthand calls
    }
}
