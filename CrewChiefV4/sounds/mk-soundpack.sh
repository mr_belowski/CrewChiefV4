#!/bin/bash

if [ "$1" = "" ] ; then
    echo "source commit must be provided"
    exit 1
fi
BASE=$1

# TODO this doesn't seem to support unicode chars and some files never got
# released in a sound pack as a result.

# makes a zip file named $1, using the provided $2 directory restriction
mk_zip() {
    # updates.txt is interpreted by UpdateHelper.ProcessFileUpdates
    > updates.txt
    git add updates.txt
    echo "creating $1 from updates to ${@:2}"
    if [ -f "$1" ] ; then
        "file exists, not updating $1"
        exit 1
    fi

    # first we need to get the list of all files deleted (or moved) since the
    # previous commit, and then "touch" those files so that we get zero valued
    # entries on disk. That's how the updater deletes files.
    for f in $(git diff --relative --name-status $BASE ${@:2} | grep -E '^(R100|D)' | cut -f2) ; do
        echo "$f"
        mkdir -p "$(dirname "$f")"
        # move is not supported
        echo "delete|$f" | sed 's|/|\\|g' >> "updates.txt"
    done
    git diff --relative --name-status $BASE ${@:2} | grep -E '^(A|M)' | cut -f2 | zip -9 "$1" -@
}

mk_zip update_sound_pack.zip {alt,background_sounds,composite_personalisation_stubs,fx,pace_notes,voice,sound_pack_version_info.txt}
mk_zip update_driver_names.zip driver_names
cd personalisations
mk_zip ../update_personalisations.zip .
cd ..
