﻿using System;
using System.Security.Cryptography;
using System.Text;
using System.IO;
using Microsoft.Win32;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using System.Windows.Forms;
using Newtonsoft.Json;
using System.Xml;
using System.Runtime.InteropServices;

namespace CrewChiefV4
{
    class PluginInstaller
    {
        Boolean messageBoxPresented;
        Boolean errorMessageBoxPresented;
        Boolean messageBoxResult;
        private readonly String rf2PluginFileName = "rFactor2SharedMemoryMapPlugin64.dll";

        private const string accBroadcastFileContents = "{\n    \"updListenerPort\": 9000,\n    \"connectionPassword\": \"asd\",\n    \"commandPassword\": \"\"\n}";

        public PluginInstaller()
        {
            messageBoxPresented = false;
            messageBoxResult = false;
        }

        private string checkMD5(string filename)
        {
            using (var md5 = MD5.Create())
            {
                using (var stream = File.OpenRead(filename))
                {
                    return BitConverter.ToString(md5.ComputeHash(stream));
                }
            }
        }

        //referance https://github.com/Jamedjo/RSTabExplorer/blob/master/RockSmithTabExplorer/Services/RocksmithLocator.cs
        private string getSteamFolder()
        {
            string steamInstallPath = "";
            try
            {
                RegistryKey steamKey = Registry.LocalMachine.OpenSubKey(@"Software\Valve\Steam") ?? Registry.LocalMachine.OpenSubKey(@"Software\Wow6432Node\Valve\Steam");
                if (steamKey != null)
                {
                    steamInstallPath = steamKey.GetValue("InstallPath").ToString();
                }
            }
            catch (Exception e)
            {
                Log.Exception(e, "Exception getting steam folder: ");
            }
            return steamInstallPath;
        }

        private List<string> getSteamLibraryFolders()
        {
            List<string> folders = new List<string>();

            string steamFolder = getSteamFolder();
            if (Directory.Exists(steamFolder))
            {
                folders.Add(steamFolder);
                string configFile = Path.Combine(steamFolder, @"config\config.vdf");

                if (File.Exists(configFile))
                {
                    Regex regex = new Regex("BaseInstallFolder[^\"]*\"\\s*\"([^\"]*)\"");
                    using (StreamReader reader = new StreamReader(configFile))
                    {
                        string line;
                        while ((line = reader.ReadLine()) != null)
                        {
                            Match match = regex.Match(line);
                            if (match.Success)
                            {
                                folders.Add(Regex.Unescape(match.Groups[1].Value));
                            }
                        }
                    }
                }

            }
            return folders;
        }

        private Boolean presentInstallMessagebox(string gameName)
        {
            if (messageBoxPresented == false)
            {
                messageBoxPresented = true;
                if (DialogResult.OK == MessageBox.Show(Configuration.getUIString("install_plugin_popup_text") + " " + gameName, Configuration.getUIString("install_plugin_popup_title"),
                    MessageBoxButtons.OKCancel, MessageBoxIcon.Information))
                {
                    messageBoxResult = true;
                }
            }
            return messageBoxResult;
        }

        private Boolean presentEnableMessagebox()
        {
            if (DialogResult.OK == MessageBox.Show(Configuration.getUIString("install_plugin_popup_enable_text"), Configuration.getUIString("install_plugin_popup_enable_title"),
                MessageBoxButtons.OKCancel, MessageBoxIcon.Information))
            {
                return true;
            }
            return false;
        }

        private Boolean presentCreateMessagebox()
        {
            if (DialogResult.OK == MessageBox.Show(Configuration.getUIString("install_plugin_popup_create_text"), Configuration.getUIString("install_plugin_popup_create_title"),
                    MessageBoxButtons.OKCancel, MessageBoxIcon.Information))
            {
                return true;
            }
            return false;
        }

        private void presentInstallUpdateErrorMessagebox(string errorText)
        {
            MessageBox.Show(Configuration.getUIString("install_plugin_popup_error_text") + Environment.NewLine + errorText, Configuration.getUIString("install_plugin_popup_error_title"),
                MessageBoxButtons.OK, MessageBoxIcon.Error);

            // Make sure we wil retry again if user does not restart CC.
            messageBoxPresented = false;
        }

        /// <summary>
        /// Check whether the plugin is installed and up to date
        /// </summary>
        /// <returns>false if it's not installed</returns>
        private bool installOrUpdatePlugin(string source, string destination, string gameName)
        {
            bool result = false;
            //I stole this from the internetz(http://stackoverflow.com/questions/3201598/how-do-i-create-a-file-and-any-folders-if-the-folders-dont-exist)
            try
            {
                string[] files = null;

                if (destination[destination.Length - 1] != Path.DirectorySeparatorChar)
                {
                    destination += Path.DirectorySeparatorChar;
                }

                if (!Directory.Exists(destination))
                {
                    Directory.CreateDirectory(destination);
                }

                files = Directory.GetFileSystemEntries(source);
                foreach (string element in files)
                {
                    // Sub directories                    
                    if (Directory.Exists(element))
                    {
                        result = installOrUpdatePlugin(element, destination + Path.GetFileName(element), gameName);
                    }
                    else
                    {
                        // Files in directory
                        string destinationFile = destination + Path.GetFileName(element);
                        //if the file exists we will check if it needs updating
                        if (File.Exists(destinationFile))
                        {
                            var ccMD5 = checkMD5(element);
                            var presentMD5 = checkMD5(destinationFile);
                            if (!ccMD5.Equals(presentMD5))
                            {
                                Log.Verbose($"New plugin checksum {ccMD5}, present plugin {presentMD5}");
                                //ask the user if they want to update the plugin
                                if (presentInstallMessagebox(gameName))
                                {
                                    File.Copy(element, destinationFile, true);
                                    Log.Commentary("Updated plugin file: " + destinationFile);
                                    Log.Verbose($"Updated plugin checksum {checkMD5(destinationFile)}");
                                }
                                else
                                {
                                    Log.Warning($"User chose not to update '{destinationFile}'");
                                }
                            }
                            else
                            {
                                Log.Commentary($"Plugin '{destinationFile}' already present");
                            }
                        }
                        else
                        {
                            if (presentInstallMessagebox(gameName))
                            {
                                File.Copy(element, destinationFile, true);
                                Log.Commentary("Installed plugin file: " + destinationFile);
                            }
                        }
                        result = true;
                    }
                }
            }
            catch (Exception e)
            {
                Log.Exception(e, "Failed to Copy plugin files: ");
                presentInstallUpdateErrorMessagebox(e.Message);
            }
            return result;
        }

        public void InstallOrUpdatePlugins(GameDefinition gameDefinition)
        {
            //gameInstallPath is also used to check if the user already was asked to update
            string gameInstallPath = "";
            string gameInstallPathPropertyName = null;
            string myDocuments = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
            switch (gameDefinition.gameEnum)
            {
                case GameEnum.ACC:
                    {
                        string content = "[file not found]";
                        // treading as lightly as possible, use the same encoding that the game is using (unicode LE, no BOM)
                        Encoding LEunicodeWithoutBOM = new UnicodeEncoding(false, false);
                        bool writeBroadcastFile = true;
                        var broadcastPath = Path.Combine(myDocuments,
                                "Assetto Corsa Competizione",
                                "Config",
                                "broadcasting.json");
                        if (File.Exists(broadcastPath))
                        {
                            try
                            {
                                // again, treading as lightly as possible read the file content without locking allowing for the file being locked by the game
                                using (FileStream fileStream = new FileStream(
                                    broadcastPath,
                                    FileMode.Open,
                                    FileAccess.Read,
                                    FileShare.ReadWrite))
                                {
                                    using (StreamReader streamReader = new StreamReader(fileStream, LEunicodeWithoutBOM))
                                    {
                                        content = streamReader.ReadToEnd();
                                        if (accBroadcastFileContents.Equals(content))
                                        {
                                            Console.WriteLine("ACC broadcast file has expected contents");
                                            writeBroadcastFile = false;
                                        }
                                    }
                                }
                            }
                            catch (Exception ex)
                            {
                                Console.WriteLine("Exception getting broadcast.json: " + ex.Message);
                            }
                        }
                        if (writeBroadcastFile)
                        {
                            try
                            {
                                // if the game is running it'll need to be bounced to pick up this change
                                if (Utilities.IsGameRunning(gameDefinition.processName, gameDefinition.alternativeProcessNames, out var parentDir))
                                {
                                    MessageBox.Show("broadcasting.json needs to be updated and the game restarted. Please exit the game then click 'OK'");
                                }
                                Console.WriteLine("Updating ACC broadcast file");
                                Console.WriteLine("Expected content:");
                                Console.WriteLine(accBroadcastFileContents);
                                Console.WriteLine("Actual content:");
                                Console.WriteLine(content);
                                // again, write with the same encoding the game uses
                                File.WriteAllText(broadcastPath, accBroadcastFileContents, LEunicodeWithoutBOM);
                            }
                            catch (Exception e) { Log.Exception(e); }
                        }
                        return;
                    }

                case GameEnum.RF2_64BIT:
                    gameInstallPathPropertyName = "rf2_install_path";
                    break;
                case GameEnum.LMU:
                    gameInstallPathPropertyName = "lmu_install_path";
                    break;
                case GameEnum.ASSETTO_32BIT:
                case GameEnum.ASSETTO_64BIT:
                case GameEnum.ASSETTO_64BIT_RALLY:
                case GameEnum.ASSETTO_PRO:
                    gameInstallPathPropertyName = "acs_install_path";
                    break;
                case GameEnum.ASSETTO_EVO:
                    gameInstallPathPropertyName = "ace_install_path";
                    break;
                case GameEnum.RF1:
                    //special case here, will figure something clever out so we dont need to have Dan's dll included in every plugin folder.
                    switch (gameDefinition.lookupName)
                    {
                        case "automobilista":
                            {
                                gameInstallPathPropertyName = "ams_install_path";
                                break;
                            }
                        case "rFactor1":
                            {
                                gameInstallPathPropertyName = "rf1_install_path";
                                break;
                            }
                        case "asr":
                            {
                                gameInstallPathPropertyName = "asr_install_path";
                                break;
                            }
                        default:
                            {
                                // this is an rFactor based game that's not rFactor or AMS (so it's fTruck, Marcas or GSC) - no automatic installation of
                                // plugin for these old games
                                Log.Error("Auto-install of plugin not supported for " + gameDefinition.friendlyName);
                                Log.Error("Assuming that the plugin in install folder" +
                                    " (default location C:\\Program Files(x86)\\Britton IT Ltd\\CrewChiefV4\\plugins\\rFactor\\Plugins) has been copied to your game's install folder");
                                return;
                            }
                    }
                    break;
                case GameEnum.RBR:
                    gameInstallPathPropertyName = "rbr_install_path";
                    break;
                case GameEnum.DIRT:
                    UpdateDirtRallyXML(myDocuments + @"\My Games\DiRT Rally\hardwaresettings\hardware_settings_config.xml",
                            UserSettings.GetUserSettings().getInt("dirt_rally_udp_data_port"));
                    return;
                case GameEnum.DIRT_2:
                    UpdateDirtRallyXML(myDocuments + @"\My Games\DiRT Rally 2.0\hardwaresettings\hardware_settings_config.xml",
                                    UserSettings.GetUserSettings().getInt("dirt_rally_2_udp_data_port"));
                    UpdateDirtRallyXML(myDocuments + @"\My Games\DiRT Rally 2.0\hardwaresettings\hardware_settings_config_vr.xml",
                        UserSettings.GetUserSettings().getInt("dirt_rally_2_udp_data_port"));
                    return;
                case GameEnum.GTR2:
                    gameInstallPathPropertyName = "gtr2_install_path";
                    break;
                case GameEnum.IRACING:
                    UpdateIracingMaxCars();
                    break;
            }

            if (String.IsNullOrEmpty(gameInstallPathPropertyName))
            { // Nothing more required
                return;
            }
            
            // the game has a plugin
            gameInstallPath = UserSettings.GetUserSettings().getPath(gameInstallPathPropertyName);
            if (!Directory.Exists(gameInstallPath))
            {
                //Present a messagebox to the user asking if they want to install plugins
                if (presentInstallMessagebox(gameDefinition.friendlyName))
                {   // First try to get the install folder from steam common install folders.
                    List<string> steamLibs = getSteamLibraryFolders();
                    foreach (string lib in steamLibs)
                    {
                        string commonPath = Path.Combine(lib, @"steamapps\common\" + gameDefinition.gameInstallDirectory);
                        if (Directory.Exists(commonPath))
                        {
                            gameInstallPath = commonPath;
                            Log.Commentary($"Install path for {gameDefinition.friendlyName} not given, found '{gameInstallPath}'");
                            break;
                        }
                    }
                    if (!Directory.Exists(gameInstallPath))
                    {   //Not found in steam folders ask the user to locate the directory
                        FolderBrowserDialog dialog = new FolderBrowserDialog();
                        dialog.ShowNewFolderButton = false;
                        dialog.Description = Configuration.getUIString("install_plugin_select_directory_start") + " " +
                                             gameDefinition.gameInstallDirectory + " " + Configuration.getUIString("install_plugin_select_directory_end") +
                                             " '" + gameInstallPath + "')";
                        dialog.RootFolder = Environment.SpecialFolder.MyComputer;

                        DialogResult result = dialog.ShowDialog();

                        if (result == DialogResult.OK && dialog.SelectedPath.Length > 0)
                        {
                            //This should now take care of checking against the main .exe instead of the folder name, special case for rFactor 2 as its has the file installed in ..\Bin64
                            if (Game.RF2_64BIT)
                            {
                                if (File.Exists(Path.Combine(dialog.SelectedPath, @"Bin64", gameDefinition.processName + ".exe")))
                                {
                                    gameInstallPath = dialog.SelectedPath;
                                }
                            }
                            else if (File.Exists(Path.Combine(dialog.SelectedPath, gameDefinition.processName + ".exe")))
                            {
                                gameInstallPath = dialog.SelectedPath;
                            }
                            else
                            {
                                //present again if user didn't select the correct folder 
                                InstallOrUpdatePlugins(gameDefinition);
                            }
                        }
                        else if (result == DialogResult.Cancel)
                        {
                            return;
                        }
                    }
                }
            }

            if (Directory.Exists(gameInstallPath))
            { //we have a gameInstallPath so we can go on with installation/updating assuming that the user wants to enable the plugin.
                if (installOrUpdatePlugin(
                        Path.Combine(DataFiles.getDefaultFolderLocation("plugins"),
                            gameDefinition.gameInstallDirectory),
                        gameInstallPath,
                        gameDefinition.friendlyName))
                { // Set path Property and edit config even if already installed
                    switch (gameDefinition.gameEnum)
                    {
                        case GameEnum.RF2_64BIT:
                        case GameEnum.LMU:
                        {
                            try
                            {
                                DLLcheck();
                                const string configFile = "CustomPluginVariables.JSON";
                                string configPath = Path.Combine(gameInstallPath, @"UserData\player", configFile);
                                Dictionary<string, Dictionary<string, int>> plugins = new Dictionary<string, Dictionary<string, int>>();
                                string json = null;
                                if (!File.Exists(configPath))
                                {
                                    if (presentCreateMessagebox())
                                    {
                                        plugins.Add(rf2PluginFileName, new Dictionary<string, int>() { { " Enabled", 1 } });
                                        json = JsonConvert.SerializeObject(plugins, Newtonsoft.Json.Formatting.Indented);
                                        File.WriteAllText(configPath, json);
                                        Log.Commentary($"{rf2PluginFileName} {configFile} created");
                                    }
                                    else
                                    {
                                        Log.Warning($"User chose not to create {rf2PluginFileName}");
                                    }
                                }
                                if (File.Exists(configPath))
                                {
                                    Log.Commentary($"Config file '{configPath}' found");
                                    json = File.ReadAllText(configPath);
                                    plugins = JsonConvert.DeserializeObject<Dictionary<string, Dictionary<string, int>>>(json);
                                    Dictionary<string, int> plugin = null;
                                    if (plugins.TryGetValue(rf2PluginFileName, out plugin))
                                    {
                                        Log.Commentary($"{rf2PluginFileName} found in {configFile}");
                                        //the whitespace is intended, this is how the game writes it.
                                        if (plugin[" Enabled"] != 1)
                                        {
                                            Log.Warning("\" Enabled\" not 1");
                                            if (presentEnableMessagebox())
                                            {
                                                plugin[" Enabled"] = 1;
                                                json = JsonConvert.SerializeObject(plugins, Newtonsoft.Json.Formatting.Indented);
                                                File.WriteAllText(configPath, json);
                                                Log.Commentary("\" Enabled\" set to 1");
                                            }
                                            else
                                            {
                                                Log.Warning("User chose not to set \" Enabled\"");
                                            }
                                        }
                                        else
                                        {
                                            Log.Commentary("\" Enabled\" already set to 1");
                                        }
                                    }
                                    else
                                    {
                                        Log.Warning($"{rf2PluginFileName} not found in {configFile}");
                                        if (presentEnableMessagebox())
                                        {
                                            plugins.Add(rf2PluginFileName, new Dictionary<string, int>() { { " Enabled", 1 } });
                                            json = JsonConvert.SerializeObject(plugins, Newtonsoft.Json.Formatting.Indented);
                                            File.WriteAllText(configPath, json);
                                            Log.Commentary($"{rf2PluginFileName} added to {configFile} with \" Enabled\" set to 1");
                                        }
                                        else
                                        {
                                            Log.Warning($"User chose not to add {rf2PluginFileName}");
                                        }
                                    }
                                }
                                else
                                {
                                    Log.Error($"Config file '{configPath}' not found");
                                }
                            }
                            catch (Exception e)
                            {
                                Log.Exception(e, "Failed to enable plugin");
                            }

                            break;
                        }

                        case GameEnum.ASSETTO_32BIT:
                        case GameEnum.ASSETTO_64BIT:
                        case GameEnum.ASSETTO_64BIT_RALLY:
                        case GameEnum.ASSETTO_PRO:
                        {
                            UserSettings.GetUserSettings().setProperty("acs_install_path", gameInstallPath);
                            string pythonConfigPath = Path.Combine(myDocuments, @"Assetto Corsa\cfg", @"python.ini");
                            if (File.Exists(pythonConfigPath))
                            {
                                string valueActive = Utilities.ReadIniValue("CREWCHIEFEX", "ACTIVE", pythonConfigPath, "0");
                                if (!valueActive.Equals("1"))
                                {
                                    if (presentEnableMessagebox())
                                    {
                                        Utilities.WriteIniValue("CREWCHIEFEX", "ACTIVE", "1", pythonConfigPath);
                                    }
                                }
                            }

                            break;
                        }
                        case GameEnum.ASSETTO_EVO:
                        {
                            UserSettings.GetUserSettings().setProperty("ace_install_path", gameInstallPath);
                            string pythonConfigPath = Path.Combine(myDocuments, @"ACE\cfg", @"python.ini");
                            if (File.Exists(pythonConfigPath))
                            {
                                string valueActive = Utilities.ReadIniValue("CREWCHIEFEX", "ACTIVE", pythonConfigPath, "0");
                                if (!valueActive.Equals("1"))
                                {
                                    if (presentEnableMessagebox())
                                    {
                                        Utilities.WriteIniValue("CREWCHIEFEX", "ACTIVE", "1", pythonConfigPath);
                                    }
                                }
                            }

                            break;
                        }
                    }
                    if (gameInstallPath != UserSettings.GetUserSettings().getPath(gameInstallPathPropertyName))
                    {
                        UserSettings.GetUserSettings().setProperty(gameInstallPathPropertyName, gameInstallPath);
                        UserSettings.GetUserSettings().saveUserSettings();
                        Log.Commentary($"Updated install path for {gameDefinition.friendlyName} to '{gameInstallPath}'");
                    }
                }
                // else it isn't installed
            }
        }

        private void UpdateIracingMaxCars()
        {
            if (iRacing.iRacingSharedMemoryReader.ReadNumberOfCarsEnabled() < 64)
            {
                var choice = MessageBox.Show(
                    $"iRacing 'Max Cars' is not set correctly for Crew Chief.\n\n" +
                    $"If you click 'OK' Crew Chief will automatically update your iRacing " + 
                    $"{DataFiles.iRacingAppIni} file (iRacing must not be active for this to work).\n\n" +
                    "Alternatively, in iRacing, go into the GRAPHICS tab under OPTIONS, " +
                    "and change 'Max Cars' to 64 (the default is 20). It will not actually " +
                    "affect your graphics performance.\n\n" +
                    "This warning will continue until 'Max Cars' is changed, because Crew Chief " +
                    "will not work properly in really strange ways that you won't be able to explain.",
                         
                    "Crew Chief needs a change to iRacing",
                    MessageBoxButtons.OKCancel, MessageBoxIcon.Information);
                if (choice == DialogResult.OK)
                {
                    // 63 is not a typo. The value is off-by-one in the config file.
                    Utilities.WriteIniValue("Graphics", "serverTransmitMaxCars", "63                \t; Limit number of cars transmitted to client (values from 10 to 64) (edited by Crew Chief)", DataFiles.iRacingAppIni);
                }
            }
        }

        private void UpdateDirtRallyXML(string fileName, int udpPort)
        {
            if (File.Exists(fileName))
            {
                try
                {
                    bool save = false;
                    XmlDocument doc = new XmlDocument();
                    doc.Load(fileName);
                    XmlNode root = doc.DocumentElement;
                    XmlNode udpNode = root.SelectSingleNode("descendant::udp");
                    if (udpNode == null)
                    {
                        // no UDP node, create it and it's motion_platform parent, with the attributes we need
                        save = true;
                        CreateDirtRallyUDPElement(doc, root, udpPort);
                    }
                    else
                    {
                        // check the attributes and update them if necessary
                        save = UpdateDirtRallyUDPAttributes(udpNode, udpPort);
                    }
                    if (save)
                    {
                        Console.WriteLine("Updating UDP element in " + fileName);
                        if (!File.Exists(fileName + "_backup"))
                        {
                            File.Copy(fileName, fileName + "_backup");
                        }
                        doc.Save(fileName);
                    }
                }
                catch (Exception e)
                {
                    Log.Exception(e, "Failed to update settings XML file " + fileName + ", ");
                }
            }
        }

        private bool UpdateDirtRallyUDPAttributes(XmlNode udpNode, int udpPort)
        {
            bool save = false;
            if (udpNode.Attributes["enabled"] == null || !udpNode.Attributes["enabled"].Value.Equals("true"))
            {
                save = true;
                udpNode.Attributes["enabled"].Value = "true";
            }
            if (udpNode.Attributes["extradata"] == null || !int.TryParse(udpNode.Attributes["extradata"].Value, out var edv) || edv < 3)
            {
                // extradata doesn't exist, or it exists but it's not set to "3" or above
                save = true;
                udpNode.Attributes["extradata"].Value = "3";
            }
            if (udpNode.Attributes["port"] == null || !udpNode.Attributes["port"].Value.Equals(udpPort.ToString()))
            {
                save = true;
                udpNode.Attributes["port"].Value = udpPort.ToString();
            }
            return save;
        }

        private void CreateDirtRallyUDPElement(XmlDocument doc, XmlNode root, int udpPort)
        {
            // try to create it
            XmlNode motionPlatform = root.SelectSingleNode("descendant::motion_platform");
            if (motionPlatform == null)
            {
                motionPlatform = doc.CreateElement("motion_platform");
                root.AppendChild(motionPlatform);
            }
            XmlNode udpNode = doc.CreateElement("udp");
            XmlAttribute enabledAttrib = doc.CreateAttribute("enabled");
            enabledAttrib.Value = "true";
            udpNode.Attributes.Append(enabledAttrib);
            XmlAttribute extradataAttrib = doc.CreateAttribute("extradata");
            extradataAttrib.Value = "3";
            udpNode.Attributes.Append(extradataAttrib);
            XmlAttribute ipAttrib = doc.CreateAttribute("ip");
            ipAttrib.Value = "127.0.0.1";
            udpNode.Attributes.Append(ipAttrib);
            XmlAttribute portAttrib = doc.CreateAttribute("port");
            portAttrib.Value = udpPort.ToString();
            udpNode.Attributes.Append(portAttrib);
            XmlAttribute delayAttrib = doc.CreateAttribute("delay");
            delayAttrib.Value = "1";
            udpNode.Attributes.Append(delayAttrib);
            motionPlatform.AppendChild(udpNode);
        }


        [DllImport("kernel32.dll", SetLastError = true, CharSet = CharSet.Auto)]
        private static extern IntPtr LoadLibrary(string lpFileName);

        [DllImport("kernel32.dll", SetLastError = true)]
        private static extern bool FreeLibrary(IntPtr hModule);
        private static bool DLLcheck()
        {
            bool result = true;
            if (UserSettings.GetUserSettings().getBoolean("enable_lmu_rf2_plugin_dll_check"))
            {
                string[] dllNames = { "ADVAPI32.dll", "MSVCR120.dll", "KERNEL32.dll" };

                foreach (string dllName in dllNames)
                {
                    // Try to load the DLL
                    IntPtr handle = LoadLibrary(dllName);

                    if (handle != IntPtr.Zero)
                    {
                        Log.Verbose($"{dllName} is installed and loaded successfully.");
                        FreeLibrary(handle); // Free the library if it was successfully loaded
                    }
                    else
                    {
                        Log.Warning($"{dllName} not found, plugin may not work");
                        if (DialogResult.No == MessageBox.Show(Configuration.getUIString("Plugin probably won't work, continue anyway?"), Configuration.getUIString($"{dllName} not found"), MessageBoxButtons.YesNo, MessageBoxIcon.Stop))
                        {
                            MainWindow.instance.Close();
                        }
                        Log.Warning($"{dllName} not found, plugin may not work, continuing anyway");
                        result = true;
                    }
                }
            }
            return result;
        }
    }
}
